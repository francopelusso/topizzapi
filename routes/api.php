<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::permanentRedirect('/', 'pizzas');

Route::get('pizzas', 'PizzaController@list');
Route::get('pizzas/{pizza}', 'PizzaController@show');
Route::post('pizzas', 'PizzaController@store')->middleware('staff');
Route::put('pizzas/{pizza}', 'PizzaController@update')->middleware('staff');
Route::delete('pizzas/{pizza}', 'PizzaController@delete')->middleware('staff');

Route::get('orders', 'OrderController@list')->middleware('auth:api');
Route::get('orders/{order}', 'OrderController@show')->middleware('auth:api');
Route::post('orders', 'OrderController@store');
Route::put('orders/{order}', 'OrderController@update')->middleware('auth:api');
Route::delete('orders/{order}', 'OrderController@delete')->middleware('staff');

Route::post('register', 'UserController@store');
