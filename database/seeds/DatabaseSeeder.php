<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    protected $tablesToTruncate = ['pizza_order'];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->tablesToTruncate as $table) {
            DB::table($table)->truncate();
        }

        $this->call(PizzaTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(OrderTableSeeder::class);
    }
}
