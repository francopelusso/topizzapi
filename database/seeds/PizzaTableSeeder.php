<?php

use Illuminate\Database\Seeder;
use App\Pizza;

class PizzaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pizza::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 50; $i++) {
            Pizza::create([
                'name' => $faker->sentence,
                'description' => $faker->paragraph,
                'price' => $faker->randomNumber,
                'image_url' => $faker->imageUrl,
                'common_score' => $faker->randomNumber
            ]);
        }
    }
}
