<?php

use Illuminate\Database\Seeder;
use App\Order;
use App\Pizza;
use App\User;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::truncate();

        // Get pizzas a single time and not twice for better performance.
        $pizzas = Pizza::orderBy('common_score', 'desc')
                    ->take(10)
                    ->get();

        //$this->createOrders(150, false, $pizzas);
        $this->createOrders(150, true, $pizzas);
    }

    /**
     * Create n orders, attach pizza m2m relation and user if specified.
     * 
     * @param int $n Number of orders to create.
     * @param bool $has_user Should attach order to a user?
     * @param Illuminate\Database\Eloquent\Collection $pizzas Pizza instances.
     * 
     * @return void
     */
    public function createOrders(int $n, bool $has_user,
        Illuminate\Database\Eloquent\Collection $pizzas
    ) {
        if ($has_user) {
            $users = User::pluck('id')->toArray();
        }
        else {
            $users = array();
        }

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < $n; $i++) {
            $ordered_pizza = $pizzas[rand(0, $pizzas->count()-1)];
            $user_id = $this->getUser($users);

            $order = Order::create([
                'price' => $ordered_pizza->price,
                'address' => $faker->address,
                'phone_number' => $faker->e164PhoneNumber,
                'user_id' => $user_id,
                'person_name' => $this->getName($faker, $user_id)
            ]);

            $order->pizzas()->attach($ordered_pizza);
        }
    }

    /**
     * Get user id for order of null if order hasn't got any users.
     * 
     * @param array $users Int array or empty array if not user.
     * 
     * @return int|null The user's id or null if not set.
     */
    public function getUser(array $users)
    {
        if (!$users) return null;

        return $users[array_rand($users)];
    }

    /**
     * Get person name from user or fake it.
     * 
     * @param int|null $user_id
     * @param \Faker\Generator $faker
     * @return string
     */
    public function getName(\Faker\Generator $faker, int $user_id = null)
    {
        if (!$user_id) return $faker->name;

        return User::find($user_id)->name;
    }
}
