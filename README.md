# ToPizzApi

REST API to use together with topizzapi-front repo for a website to order pizzas.

## Deployment

1. Install php7.2 (e.g. `sudo apt-get install php7.2`)
1. Edit `/etc/php/7.2/fpm/php.ini`: uncomment **extension=pdo_sqlite**.
1. Clone this repository and move it to **/var/www/html/**.
    ```bash
        $ git clone https://gitlab.com/francopelusso/topizzapi
        $ sudo mv topizzapi /var/www/html/
    ```
1. Go to **/var/www/html/topizzapi/** and create file **.env** copying **.env.example** and filling it in with the correct values.
    ```bash
        $ cd /var/www/html/topizzapi
        $ sudo cp .env.example .env
        $ sudo nano .env
    ```
1. Create and configure database. To use sqlite follow these steps:
    ```bash
        $ cd /var/www/html/topizzapi
        $ sudo touch database/database.sqlite
    ```
1. Run migrations:
    ```bash
        $ sudo php artisan migrate --step
    ```
1. Set files' ownership and permissions:
    ```bash
        $ cd /var/www/html/topizzapi
        $ sudo chown -R www-data:www-data .
        $ sudo chmod -R 755 ./storage
        $ sudo chmod -R 755 ./bootstrap/cache
    ```
1. Install nginx (e.g. `sudo apt-get install nginx`).
1. Configure nginx using this configuration file as a base:
    ```nginx
        server {
            server_name MYDOMAIN.com;
            root /var/www/html/topizzapi/public;

            add_header X-Frame-Options "SAMEORIGIN";
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Content-Type-Options "nosniff";

            index index.html index.htm index.php;

            charset utf-8;

            location / {
                try_files $uri $uri/ /index.php?$query_string;
            }

            location = /favicon.ico { access_log off; log_not_found off; }
            location = /robots.txt  { access_log off; log_not_found off; }

            error_page 404 /index.php;

            location ~ \.php$ {
                fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
                include fastcgi_params;
            }

            location ~ /\.(?!well-known).* {
                deny all;
            }
        }
    ```
1. Run `sudo service nginx reload`.
1. That's all. Load your data or use `php artisan db:seed` to fill with dummy.

## Creating passport client secret

1. Move to installation directory and run `php artisan passport:install`:
    ```bash
        $ cd /var/www/html/topizzapi
        $ php artisan passport:install
    ```
    **Client ID and Secret for Password grant should be output.**
