<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Order;
use App\Pizza;

class OrderTest extends TestCase
{
    /**
     * Test orders endpoint.
     *
     * @return void
     */
    public function testOrders()
    {
        $response = $this->json('GET', '/orders/');
        $response->assertStatus(401);

        $token = User::find(2)->createToken('name')->accessToken;
        $response = $this->withHeaders([
            'Authorization' => "Bearer $token"
        ])->json('GET', '/orders/');

        $response
            ->assertStatus(200)
            ->assertJsonCount(15, 'data');
    }

    /**
     * Test single order endpoint.
     *
     * @return void
     */
    public function testOrder()
    {
        $response = $this->json('GET', '/orders/3/');
        $response->assertStatus(401);

        $token = User::find(2)->createToken('name')->accessToken;
        $order_id = Order::where('user_id', '=', '2')
            ->take(1)
            ->get()
            ->first()->id;

        $response = $this->withHeaders([
            'Authorization' => "Bearer $token"
        ])->json('GET', "/orders/$order_id");

        $response
            ->assertStatus(200)
            ->assertJson(['id' => $order_id]);
    }

    /**
     * Test order creation endpoint.
     *
     * @return void
     */
    public function testOrderCreation()
    {
        $response = $this->json('POST', '/orders/');
        $response->assertStatus(400);

        $pizza = Pizza::find(1);

        $response = $this->json('POST', '/orders/', [
            'pizza_id' => $pizza->id,
            'person_name' => 'Test name',
            'address' => 'Nowhere Street, NY',
            'price' => 0,
            'phone_number' => '+111111111111'
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'person_name' => 'Test name',
                'address' => 'Nowhere Street, NY',
                'price' => $pizza->price,
                'phone_number' => '+111111111111'
            ]);

        $response = $this->json('POST', '/orders/', [
            'pizza_id' => $pizza->id,
            'person_name' => 'Test name',
            'address' => 'Nowhere Street, NY',
            'price' => 0,
            'phone_number' => '+111111111111',
            'user_id' => 1
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'person_name' => 'Test name',
                'address' => 'Nowhere Street, NY',
                'price' => $pizza->price,
                'phone_number' => '+111111111111',
                'user_id' => 1
            ]);
    }
}
