<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PizzaTest extends TestCase
{
    /**
     * Test pizzas endpoint.
     *
     * @return void
     */
    public function testPizzasEndpoint()
    {
        $response = $this->json('GET', '/pizzas/');

        $response
            ->assertStatus(200)
            ->assertJsonCount(15, 'data');
    }

    /**
     * Test single pizza endpoint.
     *
     * @return void
     */
    public function testPizza()
    {
        $response = $this->json('GET', '/pizzas/1/');

        $response
            ->assertStatus(200)
            ->assertJson(['id' => 1]);
    }
}
