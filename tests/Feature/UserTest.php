<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test register endpoint.
     *
     * @return void
     */
    public function testRegister()
    {
        $response = $this->json('POST', '/register/',[
            'name' => 'Test Name',
            'email' => 'email@mail.com',
            'password' => 'topizzapi'
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'accessToken',
                'expiresAt'
            ]);
    }
}
