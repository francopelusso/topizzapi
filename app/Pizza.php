<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $fillable = [
        'name', 'description', 'price', 'image_url', 'common_score'
    ];

    public function orders()
    {
        return $this->belongsToMany('App\Order', 'pizza_order', 'pizza_id');
    }

    public function delete()
    {
        $this->orders()->detach();
        parent::delete();
    }
}
