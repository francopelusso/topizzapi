<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $token = User::create([
            'name' => $request->all()['name'],
            'email' => $request->all()['email'],
            'password' => \Hash::make($request->all()['password'])
        ])->createToken("Personal token");

        return response()->json([
            'accessToken' => $token->accessToken,
            'expiresAt' => $token->token->expires_at
        ], 200);
    }
}
