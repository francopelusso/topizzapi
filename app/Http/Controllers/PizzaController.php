<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseModelController;
use Illuminate\Http\Request;
use App\Pizza;

class PizzaController extends BaseModelController
{
    protected $model = Pizza::class;
    protected $order_by = array('common_score', 'desc');
    protected $validate_fields = [
        'name' => 'required|max:255',
        'description' => 'required',
        'price' => 'required|integer',
        'image_url' => 'required|url',
        'common_score' => 'required|integer'
    ];
}
