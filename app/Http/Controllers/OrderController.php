<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model as Model;
use App\Http\Controllers\BaseModelController;
use Illuminate\Http\Request;
use App\Order;
use App\Pizza;

class OrderController extends BaseModelController
{
    protected $model = Order::class;
    protected $order_by = array('created_at', 'desc');
    protected $validate_fields = [
        'price' => 'required|integer',
        'address' => 'required',
        'phone_number' => 'required',
        'user_id' => 'nullable',
        'person_name' => 'nullable',
        'pizza_id' => 'required|integer'
    ];

    public function show(Model $model)
    {
        $user = auth('api')->user();
        if (!$user || $model->user_id != $user->id) {
            return response()->json(['errors' => 'Forbidden'], 403);
        }

        $pizzas = [];
        foreach($model->pizzas as $pizza) {
            $pizzas[] = [
                'id' => $pizza->id,
                'name' => $pizza->name,
                'description' => $pizza->description,
                'price' => $pizza->price,
                'image_url' => $pizza->image_url,
                'common_score' => $pizza->common_score
            ];
        }
        return response()->json([
            'id' => $model->id,
            'price' => $model->price,
            'person_name' => $model->person_name,
            'address' => $model->address,
            'phone_number' => $model->phone_number,
            'created_at' => $model->created_at,
            'pizzas' => $pizzas
        ], 200);
    }

    protected function getQuery()
    {
        $user = auth('api')->user();

        if(!$user) return array(true, '=', false);

        return array('user_id', '=', $user->id);
    }

    protected function hookCreatedModel(Model $model, Request $request)
    {
        $user = auth('api')->user();
        $pizza = Pizza::find($request->pizza_id);
        if ($user) $model->user_id = auth('api')->user()->id;
        $model->price = $pizza->price;
        $model->save();
        $model->pizzas()->attach($pizza);
        return $model;
    }

    protected function hasPermissions(Model $model)
    {
        $user = auth('api')->user();

        if (!$user) return false;

        return $order->user_id == $user->id || $user->is_staff;
    }
}
