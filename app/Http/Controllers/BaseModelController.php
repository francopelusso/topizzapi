<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Http\Request;
use Validator;

class BaseModelController extends Controller
{
    protected $model = Model::class;
    protected $order_by = array('id', 'desc');
    protected $paginate_by = 15;
    protected $validate_fields = array();

    public function list()
    {
        $query = $this->getQuery();

        return $this->model::where($query[0], $query[1], $query[2])
                ->orderBy($this->order_by[0], $this->order_by[1])
                ->paginate($this->paginate_by);
    }

    public function show(Model $model)
    {
        return $model;
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validate_fields);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $model = $this->model::create($request->all());

        $model = $this->hookCreatedModel($model, $request);

        return response()->json($model, 201);
    }

    protected function hookCreatedModel(Model $model, Request $request) {
        return $model;
    }

    public function update(Request $request, Model $model)
    {
        if (!$this->hasPermissions($model)) {
            return response()->json(['errors' => 'Unauthorized'], 403);
        }

        $model->update($request->all());

        return response()->json($model, 200);
    }

    public function delete(Request $request, Model $model)
    {
        if (!$this->hasPermissions($model)) {
            return response()->json(['errors' => 'Unauthorized'], 403);
        }

        $model->delete();

        return response()->json(['success' => true], 204);
    }

    protected function getQuery() {
        return array('', '', '');
    }

    protected function hasPermissions(Model $model) {
        return true;
    }
}
