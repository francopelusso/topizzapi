<?php

namespace App\Http\Middleware;

use Closure;

class AuthStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth('api')->check() && auth('api')->user()->is_staff) {
            return $next($request);
        }

        return response()->json(['errors' => 'Unauthorized'], 403);
    }
}
