<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'price', 'address', 'phone_number', 'user_id', 'person_name'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function pizzas()
    {
        return $this->belongsToMany('App\Pizza', 'pizza_order', 'order_id');
    }

    public function delete()
    {
        $this->pizzas()->detach();
        parent::delete();
    }
}
